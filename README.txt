INTRODUCTION
------------
 * This module makes service calls to the Viator API using an APIkey available 
from your account manager. The module imports their destinations catalogue with
associated data into the viator_destinations table. Using those destinations the 
module then downloads a JSON encoded file which can be mapped to a content type
using Feeds module and specifically the Feeds JSONPath Parser module
(https://drupal.org/project/feeds_jsonpath_parser).
  
INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
or further information.
  
CONFIGURATION
-------------
 * Visit admin/config/viator and add your API key, then run the import to get
the destinations from Viator on this page admin/config/viator/destinations. 
You can then run a product import to generate a JSON file which will be 
available in the public://viator/ folder. 

 * If the file does not appear, check Watchdog admin/reports/dblog and check 
the permissions on your files folder.
