<?php
/**
 * @file
 * Install file for the viator module.
 */

/**
 * Implements hook_uninstall().
 */
function viator_uninstall() {
  drupal_uninstall_schema('viator_destinations');
}

/**
 * Implements hook_schema().
 */
function viator_schema() {
  $schema = array();

  $schema['viator_destinations'] = array(
    'description' => 'Stores Viator destinations data',
    'fields' => array(
      'timeZone' => array(
        'description' => 'Destination Timezone',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'destinationType' => array(
        'description' => 'Destination Type',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'destinationName' => array(
        'description' => 'Destination Name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'destinationId' => array(
        'description' => 'Destination ID',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'parentId' => array(
        'description' => 'Parent ID',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'lookupId' => array(
        'description' => 'Lookup ID',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'latitude' => array(
        'description' => 'Destination Latitude',
        'type' => 'float',
        'not null' => FALSE,
      ),
      'longitude' => array(
        'description' => 'Destination Longitude',
        'type' => 'float',
        'not null' => FALSE,
      ),
      'selectable' => array(
        'description' => 'Selectable',
        'type' => 'varchar',
        'length' => 20,
        'not null' => FALSE,
      ),
      'defaultCurrencyCode' => array(
        'description' => 'Default currency of the destination',
        'type' => 'varchar',
        'length' => 3,
        'not null' => FALSE,
      ),
      'sortOrder' => array(
        'description' => 'Suggested sort order of the destination within the parentId.',
        'type' => 'int',
        'not null' => FALSE,
      ),
    ),
    'indexes' => array(
      'destinationId' => array('destinationId'),
    ),
    'primary key' => array('destinationId'),
  );

  return $schema;
}
