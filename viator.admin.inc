<?php
/**
 * @file
 * Administration forms for the viator module.
 */

/**
 * Form builder function for module settings.
 */
function viator_admin_settings_form() {

  // Check the local DB to see if we have any Viator destinations.
  $getexistingdestinations = viator_get_destination_count();

  // Do a count on the Viator Destinations table.
  $destination_count = $getexistingdestinations->rowCount();

  $form = array();

  $form['instructions'] = array(
    '#markup' => t('Add your Viator API key as a minimum requirement to get the Viator destinations.'),
  );

  $form['viator_apikey'] = array(
    '#type' => 'textfield',
    '#size' => '20',
    '#required' => TRUE,
    '#title' => t('What is the Viator API key for this site?'),
    '#default_value' => variable_get('viator_apikey'),
    '#description' => t("Allows access to Viator API."),
  );
  if ($destination_count > 0) {
    $form['viator_destination'] = array(
      '#type' => 'select',
      '#options' => viator_get_destination_list(),
      '#title' => t('What destination do you want to get products from?'),
      '#default_value' => variable_get('viator_destination'),
      '#description' => t("Select destination for API calls."),
    );
  }
  else {
    drupal_set_message(t('You need to import destinations at') . ' ' . l(t('admin/config/viator/destinations'), 'admin/config/viator/destinations'), 'warning');
  }

  $form['viator_currency_code'] = array(
    '#type' => 'textfield',
    '#size' => '3',
    '#required' => TRUE,
    '#title' => t('What currency code do you want to add to the call to Viators web services?'),
    '#default_value' => variable_get('viator_currency_code'),
    '#description' => t("Supported currency codes for partners with multi currency enabled: USD,GBP,EUR,AUD,CAD,NZD.
                         Note: Partners will be billed in the currency of the booking"),
  );

  $form['viator_service_url'] = array(
    '#type' => 'textfield',
    '#size' => '20',
    '#required' => TRUE,
    '#title' => t('What service URL do you want to use?'),
    '#default_value' => variable_get('viator_service_url', 'http://prelive.viatorapi.viator.com'),
    '#description' => t('If you are part of Viators prelive testing it will likely be http://prelive.viatorapi.viator.com, do not add a trailing slash'),
  );

  return system_settings_form($form);
}

/**
 * Add Viator form to import products.
 */
function viator_product_import_form($form, &$form_state) {

  if (variable_get('viator_apikey') == '' || variable_get('viator_destination') == '') {
    drupal_set_message(t('You need to set your API and your destination on the Viator setup page.') . l(t('admin/config/viator'), 'admin/config/viator'), 'warning');
  }

  $form = array();

  $form['import_products'] = array(
    '#type' => 'checkbox',
    '#title' => t('Tick to confirm you want to run the products importer'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import Products'),
  );

  return $form;
}

/**
 * Add Viator form to import products.
 */
function viator_destination_import_form($form, &$form_state) {

  if (variable_get('viator_apikey') == '') {
    drupal_set_message(t('You need to set all the details on the Viator setup page.') . l(t('admin/config/viator'), 'admin/config/viator'), 'warning');
  }

  $form = array();

  $form['import_destinations'] = array(
    '#type' => 'checkbox',
    '#title' => t('Tick to confirm you want to run the destinations importer'),
  );

  $form['rebuild_destinations'] = array(
    '#type' => 'checkbox',
    '#title' => t('Rebuild the destinations database table.'),
    '#description' => t('If your destinations table is out of date tick this box to rebuild.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import Destinations'),
  );

  return $form;
}
